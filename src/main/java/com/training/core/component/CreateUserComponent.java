package com.training.core.component;

import java.util.HashMap;
import java.util.Map;

import com.training.core.entity.User;

public class CreateUserComponent {
	
	public Map<String, Object> createUser(Map<String, Object> input){
		
		User user = new User();
		user.setFirstName(input.get("firstName").toString());
		user.setFatherName(input.get("fatherName").toString());
		user.setMotherName(input.get("motherName").toString());
		user.setDocumentType(input.get("documentType").toString());
		user.setDocumentNumber(input.get("documentNumber").toString());
		
		
		saveUser(user);
		
		Map<String, Object> output = new HashMap<>();
		output.put("user", user);
		
		return output;
	}
	
	private User saveUser(User user) {
	
		user.setId(1000L);
		user.setStatus("CREATED");
		
		return user;
	}

}
