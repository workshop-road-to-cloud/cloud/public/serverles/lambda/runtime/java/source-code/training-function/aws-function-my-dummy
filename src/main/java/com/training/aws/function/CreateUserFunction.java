package com.training.aws.function;

import java.util.Map;

import com.training.core.component.CreateUserComponent;

public class CreateUserFunction {
	
	public Map<String, Object> createUser(Map<String, Object> input){
		
		validateInputStruture(input);
		
		CreateUserComponent createUserComponent = new CreateUserComponent();
		Map<String, Object> output = createUserComponent.createUser(input);
		
		return output;
	}
	
	
	private void validateInputStruture(Map<String, Object> input) {
		System.out.println("TODO");
	}

}
