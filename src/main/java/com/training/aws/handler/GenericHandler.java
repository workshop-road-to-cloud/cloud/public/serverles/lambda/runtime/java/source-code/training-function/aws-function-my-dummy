package com.training.aws.handler;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;

public class GenericHandler {

	public Map<String, Object> handleRequest(Map<String, Object> input, Context context) {

		context.getLogger().log("Input: " + input);
		
		Map<String, Object> output = new HashMap<>();
		output.put("myKey", "Hola Mundo");
		
		return output;

	}

}
