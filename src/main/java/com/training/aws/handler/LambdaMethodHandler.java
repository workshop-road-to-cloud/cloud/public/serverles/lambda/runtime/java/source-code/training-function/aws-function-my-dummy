package com.training.aws.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;

public class LambdaMethodHandler {

	public Object handleRequest(Object input, Context context) {
		
		context.getLogger().log("Input: " + input);
		
		Map<String, Object> mapNote = new HashMap<>();
		ArrayList<Map<String, Object>> mapNotes = new ArrayList<>();
		
		mapNote = new HashMap<>();
		mapNote.put("id", 1);
		mapNote.put("title", "Git commands");
		mapNote.put("bodyMinimal", "git init<br/>Initialises a Git repository in that ");
		mapNote.put("lastUpdatedDate", "2023-05-18");
		mapNotes.add(mapNote);		
		
		mapNote = new HashMap<>();
		mapNote.put("id", 2);
		mapNote.put("title", "Learning Angular");
		mapNote.put("bodyMinimal", "git init<br/>Initialises a Git repository in that ");
		mapNote.put("lastUpdatedDate", "2023-05-18");
		mapNotes.add(mapNote);
		
		mapNote = new HashMap<>();
		mapNote.put("id", 3);
		mapNote.put("title", "Flutter Commands");
		mapNote.put("bodyMinimal", "git init<br/>Initialises a Git repository in that ");
		mapNote.put("lastUpdatedDate", "2023-05-18");
		mapNotes.add(mapNote);
		
		return mapNotes;
	}

}
