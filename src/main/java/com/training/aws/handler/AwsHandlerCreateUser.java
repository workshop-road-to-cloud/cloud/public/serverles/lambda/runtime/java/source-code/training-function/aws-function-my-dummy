package com.training.aws.handler;

import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.training.aws.function.CreateUserFunction;

public class AwsHandlerCreateUser {

	public Map<String, Object> handleRequest(Map<String, Object> input, Context context) {

		context.getLogger().log("Input: " + input);

		CreateUserFunction createUserFunction = new CreateUserFunction();

		Map<String, Object> output = createUserFunction.createUser(input);
		
		context.getLogger().log("Output: " + output);
	
		return output;

	}
}
